using System.Collections.Generic;
using GameFoundation;
using SomeGame.GameObjects;
using SomeGame.Infrastructure;

namespace SomeGame.Scenes
{
	internal class BallScene : SceneBase
	{
		private readonly List<IPlayer> _players = new List<IPlayer>();

		public BallScene(int width, int height, IKeyboardStatus keyboardStatus)
			: base(width, height, keyboardStatus)
		{
		}

		public override void Initialize()
		{
			var ball = new Ball(10, 10, 10, 10);
			var player = new Player.Player(KeyboardStatus, ball);
			_players.Add(player);
			SceneObjects.Add(ball);
		}

		public override void Update(double deltaTime)
		{
			base.Update(deltaTime);

			foreach (IPlayer player in _players)
				player.DealKeyboard();
		}
	}
}