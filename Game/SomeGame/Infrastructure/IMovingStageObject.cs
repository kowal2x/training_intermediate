namespace SomeGame.Infrastructure
{
	internal interface IMovingStageObject : ISceneObject
	{
		double VelocityX { get; }
		double VelocityY { get; }
	}
}