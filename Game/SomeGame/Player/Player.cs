using System.Windows.Forms;
using GameFoundation;
using SomeGame.Infrastructure;

namespace SomeGame.Player
{
	internal class Player : IPlayer
	{
		private readonly IVelocityChangeable _controlledObject;
		private readonly IKeyboardStatus _keyboardStatus;

		public Player(IKeyboardStatus keyboardStatus, IVelocityChangeable controlledObject)
		{
			_keyboardStatus = keyboardStatus;
			_controlledObject = controlledObject;
		}

		public void DealKeyboard()
		{
			double dvx = 0;
			double dvy = 0;

			if (_keyboardStatus.IsKeyPressed(Keys.Up))
				dvy = -10;
			if (_keyboardStatus.IsKeyPressed(Keys.Down))
				dvy = 10;
			if (_keyboardStatus.IsKeyPressed(Keys.Left))
				dvx = -10;
			if (_keyboardStatus.IsKeyPressed(Keys.Right))
				dvx = 10;

			_controlledObject.ChangeVelocity(dvx, dvy);
		}
	}
}